from random import randint
from song_category import *
from thailand_category import *
from country_category import *

title = 0     
body = 1
dashes_space = []
cur_string = ""
score = 0

category = {
    "1": ("Famous Song", song),     # category no. :("title", body)
    "2": ("Thailand", thailand),
    "3": ("Country", country),
}

def print_category():
    for no in range(len(category)):
        no = no + 1
        print(no, category[str(no)][title])

def print_hint(hint):
    print('Hint: "%s"' %(hint))

def print_screen(dashes_row, score, remain, wrong_guess):
    print(*dashes_row," score %d"%(score), ", remaining wrong guess %d"%(remain), end="")
    if wrong_guess != "":
        print(", wrong guessed: %s" %(wrong_guess) if wrong_guess!="" else "")
    else:
        print()

def select_category():
    print("Select Category:")
    print_category()
    select = input("> ")
    return category[select]

def random_word(element):
    size = len(element[body])
    no = randint(1, size)
    hint = element[body][str(no)][0]
    word = element[body][str(no)][1]
    return hint, word

def simplify_word(word):
    temp = {}
    i=0
    for char in word:
        temp[i] = char
        i+=1
    word = temp
    return word

def encrypt_dash(word):
    global dashes_space
    for i in range(len(word)):
        value = list(word.values())[i]
        if value.isalpha():
            dashes_space += '_'
        else:
            dashes_space += value
    return dashes_space

def result(remain, score):
    if remain > 0:
        print("You Win :)")
        print("Your Score : %d" %(score))
    else: print("You Lose :(")

def guess_word(hint, word, dashes_row):
    global score
    remain = 6
    wrong_guess = ""
    
    while remain > 0 :
        wrong_char = True
        temp = ""
        print_screen(dashes_row, score, remain, wrong_guess)
        guess = input('> ')[0]
        
        for i in range(len(word)):
            key = list(word.keys())[i]
            value = list(word.values())[i]
            if value.isalpha() :
                if guess == value :
                    if dashes_row[key] == "_":
                        dashes_row[key] = guess
                        score += 5
                    wrong_char = False            
                if wrong_char :
                    temp = guess
        
        if wrong_char :
            temp += ", "
            wrong_guess += temp
            remain -= 1
        if "_" in dashes_row:
            continue
        else: 
            print_screen(dashes_row, score, remain, wrong_guess)
            break

    result(remain, score)
    
 

########## Main #####################################

element = select_category()
hint, word = random_word(element)
print_hint(hint)
word = simplify_word(word)
dashes_row = encrypt_dash(word)
guess_word(hint, word ,dashes_row)



